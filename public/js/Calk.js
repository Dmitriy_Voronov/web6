function updatePrice() {
  // Находим select по имени в DOM.
  let s = document.getElementsByName("prodType");
  let select = s[0];
  let price = 0;
  let prices = getPrices();
  let priceIndex = parseInt(select.value) - 1;
  let buf1 = 0;
  let buf2 = 0;
  if (priceIndex >= 0) {
    price = prices.prodTypes[priceIndex];
  }
  
  // Скрываем или показываем радиокнопки.
  let radioDiv = document.getElementById("radios");
  radioDiv.style.display = (select.value == "2" ? "block" : "none");
  
  // Смотрим какая товарная опция выбрана.
  let radios = document.getElementsByName("prodOptions");
  radios.forEach(function(radio) {
    if (radio.checked) {
      let optionPrice = prices.prodOptions[radio.value];
      if (optionPrice !== undefined) {
        price += optionPrice;
        buf1 += optionPrice;
      }
    }
  });

  // Скрываем или показываем чекбоксы.
  let checkDiv = document.getElementById("checkboxes");
  checkDiv.style.display = (select.value == "3" ? "block" : "none");

  // Смотрим какие товарные свойства выбраны.
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function(checkbox) {
    if (checkbox.checked) {
      let propPrice = prices.prodProperties[checkbox.name];
      if (propPrice !== undefined) {
        price += propPrice;
        buf2 += propPrice;
      }
    }
  });


  if (select.value == "2")
  price=price-buf1-buf2;
  if (select.value == "1")
  price=price-buf1;
  if (select.value == "3")
  price=price-buf2;

  let num=document.getElementById("count").value;
  if (typeof num=='undefined') num=0;
  price*=num;


  let prodPrice = document.getElementById("prodPrice");
  prodPrice.innerHTML = price + " рублей";
}

function getPrices() {
  return {
    prodTypes: [399, 329, 399, 399],
    prodOptions: {
      home: 40,
    },
    prodProperties: {
      napit: 80,
      figyrka: 60,
    }
  };
}

window.addEventListener('DOMContentLoaded', function (event) {
  // Скрываем радиокнопки.
  let radioDiv = document.getElementById("radios");
  radioDiv.style.display = "none";
  
  // Находим select по имени в DOM.
  let s = document.getElementsByName("prodType");
  let select = s[0];
  // Назначаем обработчик на изменение select.
  select.addEventListener("change", function(event) {
    let target = event.target;
    console.log(target.value);
    updatePrice();
  });
  
  // Назначаем обработчик радиокнопок.  
  let radios = document.getElementsByName("prodOptions");
  radios.forEach(function(radio) {
    radio.addEventListener("change", function(event) {
      let r = event.target;
      console.log(r.value);
      updatePrice();
    });
  });

    // Назначаем обработчик радиокнопок.  
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function(checkbox) {
    checkbox.addEventListener("change", function(event) {
      let c = event.target;
      console.log(c.name);
      console.log(c.value);
      updatePrice();
    });
  });

  updatePrice();
});
// Галерея
$(document).ready(function()
{
	console.log( "ready!" );
	$('.next').click(function()
	{
		var currentImage = $('.imge.curry');
		var currentImageIndex = $('.imge.curry').index();
		var nextImageIndex = currentImageIndex + 1;
		var nextImage = $('.imge').eq(nextImageIndex);
		
		currentImage.fadeOut(1000);
		currentImage.removeClass('curry');
		
		if(nextImageIndex == ($('.imge:last').index()+1))
		{
			$('.imge').eq(0).fadeIn(1000);
			$('.imge').eq(0).addClass('curry');
		} 
		else
		{
			nextImage.fadeIn(1000);
			nextImage.addClass('curry');
		}
	});
	$('.prev').click(function()
	{
		var currentImage = $('.imge.curry');
		var currentImageIndex = $('.imge.curry').index();
		var prevImageIndex = currentImageIndex - 1;
		var prevImage = $('.imge').eq(prevImageIndex);
		
		currentImage.fadeOut(1000);
		currentImage.removeClass('curry');
		
		prevImage.fadeIn(1000);
		prevImage.addClass('curry');
	});
});